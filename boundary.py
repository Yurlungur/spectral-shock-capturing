#!/usr/bin/env python2

"""boundary.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-04-14 16:28:08 (jmiller)>

This is a part of pseudospectral_shock.py
It provides a class/interface for imposing boundary
conditions.
"""

# ======================================================================
# imports
# ======================================================================
import numpy as np
from copy import copy
import abc # The method for abstract base classes.
# ======================================================================

class BoundaryCondition:
    """
    An abstract base class for boundary conditions.
    Provides the "enforce" method, which enforces
    the boundary conditions.
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def enforce(self,grid_func):
        """
        Takes a nodal representation of a function,
        grid_func, and returns a new version with
        the boundary conditions enforced.
        """

    def enforce_over_state_vector(self,state_vector):
        """
        Enforces boundary condition over a state vector
        of grid functions
        """
        num_vars = state_vector.shape[0]
        return np.array([self.enforce(grid_func) for grid_func in state_vector])

    def __call__(self,state_vector):
        "convenience function. Calls enforce_over_state_vector"
        return self.enforce_over_state_vector(state_vector)


class DirichletBoundaryCondition(BoundaryCondition):
    """
    An implementation of Dirichlet boundary conditions.

    Demands that y(xmin) = yleft
                 y(xmax) = yright
    """
    def __init__(self,yleft,yright):
        """
        Demands that y(xmin) = yleft
                     y(xmax) = yright
        """
        self.yleft = yleft
        self.yright = yright

    def enforce(self,grid_func):
        out = copy(grid_func)
        out[0] = self.yleft
        out[-1] = self.yright
        return out

class OneSidedDirichletBoundaryCondition(BoundaryCondition):
    """
    An implementation of Dirichlet boundary conditions with only one side.

    Demands that y(xmin) = yleft
    """
    def __init__(self,yleft):
        """
        Demands that y(xmin) = yleft
                     y(xmax) = yright
        """
        self.yleft = yleft

    def enforce(self,grid_func):
        out = copy(grid_func)
        out[0] = self.yleft
        return out

class OneSidedPeriodicBoundaryCondition(BoundaryCondition):
    """
    An implementation of periodic boundary conditions
    that only uses upwind boundary data. In other words,
    imposes either left-handed or right-handed boundary data.
    """
    def __init__(self,side):
        """
        Sets the appropriate side of the domain
        to the other side

        Params
        ------
        side, string-like. Must be 'left' or 'right.'
        """
        assert type(side) == str
        if side.lower() == 'left':
            self.dest = 0
            self.source = -1
        elif side.lower() == 'right':
            self.dest = -1
            self.source = 0
        else:
            raise ValueError("Expected left or right side.")

    def enforce(self,grid_func):
        out = copy(grid_func)
        out[self.dest] = grid_func[self.source]
        return out

class TwoSidedPeriodicBoundaryCondition(BoundaryCondition):
    """
    An implementation of two-sided periodic boundary conditions.
    """
    def enforce(self,grid_func):
        out = copy(grid_func)
        out[0] = grid_func[-2]
        out[-1] = grid_func[1]
        return out

class NoBoundaryCondition(BoundaryCondition):
    def enforce(self,grid_func):
        return grid_func
