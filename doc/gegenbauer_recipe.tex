\documentclass[]{article}
% Time-stamp: <2015-04-03 21:13:49 (jmiller)>

% packages
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{mathrsfs}
\usepackage{verbatim}
\usepackage{braket}
\usepackage{listings}
\usepackage{pdfpages}
\usepackage{listings}
\usepackage{color}
\usepackage{hyperref}

% Preamble
\author{Jonah Miller}
\title{Recipe for Gegenbauer Reconstruction}

% Macros
\newcommand{\R}{\mathbb{R}}
\newcommand{\eval}{\biggr\rvert} %evaluated at
\newcommand{\N}{\mathbb{N}} % Integers
\newcommand{\myvec}[1]{\vec{#1}} % vectors for me
% total derivatives 
\newcommand{\diff}[2]{\frac{d #1}{d #2}} 
\newcommand{\dd}[1]{\frac{d}{d #1}}
% partial derivatives
\newcommand{\pd}[2]{\frac{\partial #1}{\partial #2}} 
\newcommand{\pdd}[1]{\frac{\partial}{\partial #1}} 
% second partial derivatives
\newcommand{\ppdd}[3]{\frac{\partial^2 #1}{\partial #2 \partial #3}}
% vector calculus
\newcommand{\grad}{\myvec{\nabla}}
% quantum
\newcommand{\comm}[2]{\left[#1,#2\right]}
\newcommand{\norm}[1]{\left|#1\right|}
% special relativity
\newcommand{\ltran}[2]{\pd{\bar{x}^{#1}}{x^{#2}}}
% QFT
\newcommand{\PB}[2]{\comm{#1}{#2}_{PB}} % poisson bracket
\newcommand{\Ld}{\mathcal{L}}
\newcommand{\Hd}{\mathcal{H}}
% linear algebra
\newcommand{\minkowski}{\begin{pmatrix}1&0&0&0\\0&-1&0&0\\0&0&-1&0\\0&0&0&-1\end{pmatrix}}
\newcommand{\fmunu}{\begin{pmatrix}
    0     & -E^1/c & -E^2/c & -E^3/c \\
    E^1/c & 0      & -B^3   & B^2    \\
    E^2/c & B^3    & 0      &-B^1    \\
    E^3/c & -B^2   & B^1    & 0
  \end{pmatrix}}
\newcommand{\gmunu}{\begin{pmatrix}
    0   & - B^1  & - B^2  & -B^3   \\
    B^1 & 0      & E^3/c  & -E^2/c \\
    B^2 & -E^3/c & 0      & E^1/c  \\
    B^3 & E^2/c  & -E^1/c & 0
  \end{pmatrix}}

% fractions
\newcommand{\half}{\frac{1}{2}}

% braces
\newcommand{\paren}[1]{\left( #1 \right)}
\newcommand{\sqrbrace}[1]{\left[ #1 \right]}
\newcommand{\curlybrace}[1]{\left\{ #1 \right\}}

% vectors
\newcommand{\vx}{\myvec{x}}
\newcommand{\vy}{\myvec{y}}
\newcommand{\vk}{\myvec{k}}
\newcommand{\vq}{\myvec{q}}
\newcommand{\vP}{\myvec{P}}

%operators
\newcommand{\ha}{\hat{a}}
\newcommand{\hb}{\hat{b}}
\newcommand{\hc}{\hat{c}}
\newcommand{\hd}{\hat{d}}
\newcommand{\hphi}{\hat{\phi}}
\newcommand{\hpi}{\hat{\pi}}
\newcommand{\hvphi}{\hat{\varphi}}
\newcommand{\hvpi}{\hat{\varpi}}
%conjugates
\newcommand{\had}{\hat{a}^\dagger}
\newcommand{\hbd}{\hb^\dagger}
\newcommand{\hcd}{\hc^\dagger}
\newcommand{\hdd}{\hd^\dagger}
\newcommand{\hphid}{\hat{phi}^\dagger}
\newcommand{\hpid}{\hat{pi}^\dagger}
\newcommand{\hvphid}{\hat{\varphi}^\dagger}
\newcommand{\hvpid}{\hat{\varpi}^\dagger}

% metric shorthands
\newcommand{\gij}{\gamma_{ij}}
\newcommand{\Kij}{K_{ij}}
\newcommand{\diag}{\text{diag}}

% controls
% \renewcommand\thesubsection{\thesection.\alph{subsection}}

\begin{document}
\maketitle

\section{Continuous Case}

Suppose that $f(x)$ is an analytic function on $[a,b]\subset
[-1,1]$. Given the first $(N+1)$ coeffiecients of a Fourier or series
or a polynomial expansion based on the Gegeenbauer polynomial basis
$\{G_k^\mu\}$, define the Fourier or Gegenbauer approximation
\begin{equation}
  \label{eq:def:spectral:series}
  f_N(x) = \sum_{k=0}^N \hat{f}_k \Psi_k(x)
\end{equation}
where
\begin{equation}
  \label{eq:def:spectral:coefficients}
  \hat{f}_k = \braket{f,\Psi_k}_w = \int_{-1}^1 w(x) f(x)\Psi_k(x) dx.
\end{equation}
Further define the map $\xi : [a,b] \to [-1,1]$ given by inverting the
relationship
\begin{equation}
  \label{eq:def:x:of:xi}
  x = \left(\frac{b-a}{2}\right)\xi + \left(\frac{b+a}{2}\right).
\end{equation}
Or,
\begin{equation}
  \label{eq:def:xi:of:x}
  \xi = \frac{2}{b-a}\left[x - \frac{b+a}{2}\right].
\end{equation}
Then use $f_N$ to compute the first $m+1$ Gegenbauer coefficients
\begin{equation}
  \label{eq:def:gegenbauer:continuous}
  \hat{g}^\lambda_l = \frac{1}{h^\lambda_l}\int_{-1}^1\left(1 - \xi^2\right)^{\lambda-\frac{1}{2}}f_N(x(\xi))G_l^\lambda(\xi) d\xi\text{ for }l=0,...,m
\end{equation}
where
\begin{equation}
  \label{eq:def:h}
  \|h^\lambda_l\|^2 = \int_{-1}^1 (1- x^2)^{\lambda -\frac{1}{2}}\left[G_l^\lambda(x)\right]^2 dx = \sqrt{\pi}G_l^\lambda(1)\frac{\Gamma\left(\lambda + \frac{1}{2}\right)}{\Gamma(\lambda)(l + \lambda)} = \sqrt{\pi}\left(\frac{\Gamma(l+2\lambda)}{l!\Gamma(2\lambda)}\right)\left(\frac{\Gamma\left(\lambda + \frac{1}{2}\right)}{\Gamma(\lambda)(l + \lambda)}\right).
\end{equation}
Then for $\lambda = m = \beta\epsilon N$, where $\beta < 2\pi e /27$
for the Fourier case or $\beta < 2/27$ otherwise, the spectral series
in the Gegenauer basis converges exponentially:
\begin{equation}
  \label{eq:exponential:convergence}
  \max_{-1\leq x\leq 1} \left| f(\xi) - \sum_{l=0}^m \hat{g}^\lambda_l G^\lambda_l(\xi)\right| \leq A q e^{-\epsilon N},
\end{equation}
for some $q < 1$ and some constant $A$.

\section{Discrete Case}

Just convert, post-process, and convert back.

\end{document}
