#!/usr/bin/env python2

"""time_integration.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-04-15 18:56:26 (jmiller)>

This is part of the pseudospectral_shock library.
It implements time integration.
"""

# ======================================================================
# imports
# ======================================================================
import numpy as np
import evolution_systems
from scipy import integrate
from orthopoly import PseudoSpectralStencil
# ======================================================================



# ======================================================================
# imports
# ======================================================================
CFL_FACTOR = 0.1
# ======================================================================



def get_min_dx(x):
    """
    Returns the minimum distance between grid points in the
    array x
    """
    dx = x[1:] - x[:-1]
    return np.min(dx)


    
def get_dt(x):
    """
    Given a grid x, returns the timestep we should use based
    on the global choice of CFL factor.
    """
    return CFL_FACTOR*get_min_dx(x)

    

def rk4_step(t,state_vector,dt,
             evolution_system,
             boundary_condition,
             post_processor=False):
    """
    A time-step for an RK4 integrator.

    Parameters
    ----------
    -- t, the current time
    -- state_vector, the current state of the system
    -- evolution_system, defines the right-hand-side and variables, etc.
    -- boundary_condition, if you need to impost boundary conditions at every substep,
       you can use the boundary_condition operator.
    -- post_processor is a function that post-processes the solution at every time step.
       useful for filtering.
    """
    k1 = boundary_condition(evolution_system.rhs(t,state_vector))
    k2 = boundary_condition(evolution_system.rhs(t+0.5*dt,
                                                 state_vector + 0.5*k1*dt))
    k3 = boundary_condition(evolution_system.rhs(t+0.5*dt,state_vector + 0.5*k2*dt))
    k4 = boundary_condition(evolution_system.rhs(t+dt,state_vector+k3*dt))
    ynew = boundary_condition(state_vector + (1.0/6)*dt*(k1+2*k2+2*k3+k4))
    tnew = t+dt
    if post_processor:
        ynew = post_processor(tnew,ynew)
    return tnew,ynew

    

def integrate_system(t0,tmax,
                     y0,
                     stencil,
                     evolution_system,
                     boundary_condition,
                     post_processor=False,
                     output=False):
    """
    Integrate the PDE.

    Parameters
    ----------
    -- t0, the initial time
    -- tmax, the time to integrate to
    -- y0, the inital state vector. Array-like. Rows are x. Columns are variables.
    -- stencil, the stencil defining derivative information
    -- evolution_system, the object defining the right-hand-side
    -- boundary_condition, the object defining the boundary conditions.
    """
    dt = get_dt(stencil.get_x())
    t_list = np.arange(t0,tmax+dt,dt)
    y_list = np.empty((len(t_list),len(evolution_system.state_vector()),len(stencil.get_x())))
    y_list[0] = y0
    for i,t in enumerate(t_list[1:]):
        tnew,ynew = rk4_step(t,y_list[i],dt,evolution_system,
                             boundary_condition,post_processor)
        if output:
            print tnew
        try:
            y_list[i+1] = ynew
        except:
            print ynew
            return t_list,y_list
    return t_list,y_list
        
    
    
    
