#!/usr/bin/env python2

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt

#x = np.arange(1,30)
x = np.array([2**i for i in range(1,6)])
ygud = 1./x**2
xgeg = np.floor((2./27.)*x)
ygeg = 2**(-xgeg)
xgeg2 = np.floor((2*np.pi*np.exp(1)/27.)*x)
ygeg2= 2**(-xgeg2)

mpl.rcParams.update({'font.size': 22})
plt.semilogy(x,ygud,'b-',lw=3)
#plt.semilogy(x,ygud,'ro',ms=10)
plt.xlim((x[0]-1,x[-1]+1))
plt.xlabel(r'$N_{volumes}$')
plt.ylabel('error')
plt.savefig('muscl_v_geg.pdf',bbox_inches='tight')
plt.show()

plt.semilogy(x,ygeg,'b-',lw=3)
#plt.semilogy(x,ygeg,'ro',ms=10)
plt.semilogy(x,ygeg2,'g--',lw=3)
#plt.semilogy(x,ygeg2,'ko',ms=10)
plt.xlim((x[0]-1,x[-1]+1))
plt.legend(['Chebyshev basis','Fourier basis'],loc='lower left')
plt.xlabel(r'$N_{modes}$')
plt.ylabel('error')
plt.savefig('geg_v_muscl.pdf',bbox_inches='tight')
plt.show()
