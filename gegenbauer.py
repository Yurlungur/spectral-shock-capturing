#!/usr/bin/env python2

"""
gegenbauer.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-04-14 15:03:30 (jmiller)>

This is a little module contains all the tools one needs to implement
a Gegenbauer reconstruction for a (modal or nodal) spectral method.
"""

# ======================================================================
# imports
# ======================================================================
import numpy as np
from numpy import polynomial
from scipy import integrate
from scipy.special import gegenbauer,gamma
# ======================================================================

# below this, we consider gegenbauer coefficients to vanish.
# prevents ill condititoning of Gegenbauer coefficients.
# (problem is that the solutions blow up at boundaries)
DEFAULT_CUTOFF = 1E-6
EPSILON=1E-9 # Below this number we assume a Gegenbauer coefficient is zero

# ======================================================================
# Gegenbauer Reconstruction
# ======================================================================
def get_gegenbauer_order(order):
    """
    The Gegenbauer solution must be a different order than
    the solution in the original basis.
    """
    return int(np.floor((1.9/27)*order))

def gegenbauer_norm(weight_lambda,index):
    """
    The normalization factor for the Gegenbauer polynomials
    < G_index^(weight_lambda) | G_index^(weight_lambda) >
    """
    out = np.sqrt(np.pi)
    out *= gegenbauer(index,weight_lambda)(1)
    out *= (gamma(weight_lambda+0.5)/(gamma(weight_lambda)*(weight_lambda+index)))
    return out

def get_x(a,b,xi):
    """
    Map x(xi) : [-1,1] -> [a,b]
    """
    return ((b-a)/2.0)*xi + ((b+a)/2.0)
def get_xi(a,b,x):
    """
    Map xi(x) : [a,b] -> [-1,1]
    """
    return (2.0/(b-a))*(x - ((b+a)/2.0))

def gegenbauer_weight(xi,weight_lambda):
    """
    The weight function for the Gegenbauer polynomials,
    given by weight_lambda
    """
    return (1 - xi**2)**(weight_lambda - 0.5)

def get_gegenbauer_basis(gegenbauer_order):
    """
    Given order of the Gegenbauer method, gegenbauer_order, returns an
    array containing all of the basis functions for the Gegenbauer
    solutions.
    """
    weight_lambda = gegenbauer_order
    return np.array([gegenbauer(i,weight_lambda) \
                         for i in range(gegenbauer_order+1)])

def get_gegenbauer_norms(gegenbauer_order):
    """
    Given the order of the Gegenbauer method, returns all of the
    appropriate normalization coefficients.

    (Actually returns the coefficients**(-1), since this is more useful.)
    """
    norms = np.array([gegenbauer_norm(gegenbauer_order,i)\
                          for i in range(gegenbauer_order+1)])
    return 1.0/norms

def get_gegenbauer_coeffs(f,a,b,
                          gegenbauer_order,
                          gegenbauer_norms,
                          gegenbauer_basis,
                          cutoff=DEFAULT_CUTOFF):
    """
    The projection of the function f onto every basis element in
    gegenbauer_basis (with order gegenbauer_order) with the associated
    gegenbauer_norms on the interval [a,b].
    """
    coeffs = np.empty_like(gegenbauer_basis)
    for i in range(len(coeffs)):
        G = gegenbauer_basis[i]
        w = lambda xi: gegenbauer_weight(xi,gegenbauer_order)
        integrand = lambda xi: w(xi)*G(xi)*f(get_x(a,b,xi))
        integral = integrate.quad(integrand,-1,1,
                                  epsabs=EPSILON,
                                  epsrel=10.0**(-gegenbauer_order))[0]
        coeffs[i] = gegenbauer_norms[i]*integral
    nonzero_coeffs = filter(lambda x: np.abs(x) > EPSILON, coeffs)
    if len(nonzero_coeffs) == 0:
        return coeffs
    coeffs = np.array(map(lambda x: 0\
                          if np.abs(x/nonzero_coeffs[0]) < 10**(-gegenbauer_order) \
                          or np.abs(x) < EPSILON\
                          else x,
                          coeffs))
    return coeffs

def get_gegenbauer_solution(gcoeffs,weight_lambda,a,b,gfuncs,continuum=False):
    """
    Given the coefficients for the Gegenbauer expansion of
    weight parameter lambda on interval [a,b], returns a
    a function that can be evaluated in a vectorized fashion.

    gfuncs is a list of the gegenbauer functions with the appropriate weight.
    
    Uses closures

    If continuum == True, returns a map from doubles to doubles. Otherwise
    returns a map from arrays to arrays
    """
    def gegenbauer_interpolant(x):
        xi = get_xi(a,b,x)
        g_evals = np.empty((len(gcoeffs),len(xi)),dtype=float)
        for i in range(len(gcoeffs)):
            g_evals[i] = gcoeffs[i]*gfuncs[i](xi)
        return np.sum(g_evals,axis=0)
    if continuum:
        return lambda x: gegenbauer_interpolant(np.array([x]))[0]
    else:
        return gegenbauer_interpolant
# ======================================================================


class GegenbauerReconstructor:
    """
    A convenience class. The GegenbauerReconstructor bundles
    everything the user needs to perform a Gegenbauer reconstruction
    on a continuum function on a finite interval.
    """
    def __init__(self,order):
        """
        Initializes the Reconstructor. It needs the order of the
        polynomial spectral method. From that, it saves a few
        convenience properties.
        """
        # The lambda parameter for the gegenbauer method and
        # also the number of modes used for the reconstruction.
        self.order = get_gegenbauer_order(order)
        self.norms = get_gegenbauer_norms(self.order)
        self.basis = get_gegenbauer_basis(self.order)

    def weight(x):
        """
        Returns the weight functions for this family of Gegenbauer
        polynomials
        """
        return gegenbauer_weight(x,self.norm)

    def get_poly(i):
        """
        Returns the ith Gegenbauer polynomial in this family.
        """
        return self.basis[i]

    def get_coeffs(self,f,a,b):
        """
        The projection of the function f onto every basis element in
        gegenbauer_basis (with order gegenbauer_order) with the associated
        gegenbauer_norms on the interval [a,b].
        """
        return get_gegenbauer_coeffs(f,a,b,self.order,self.norms,self.basis)

    def reconstruct_solution(self,f,a,b,interval = False):
        """
        Given a function f on the interval [a,b], returns an interpolant
        based on the Gegenbauer expansion.

        If interval is included, instead returns the interpolant evaluated
        on all the points in the interval.
        """
        coeffs = self.get_coeffs(f,a,b)
        interpolant = get_gegenbauer_solution(coeffs,
                                              self.order,
                                              a,b,
                                              self.basis)
        if np.all(interval == False):
            return interpolant
        else:
            return interpolant(interval)

    def __call__(f,a,b,interval=False):
        "Calls reconstruct_solution."
        return reconstruct_solution(f,a,b,interval)
    
