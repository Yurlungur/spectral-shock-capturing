#!/usr/bin/env python2

"""gibbs_remover.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-04-14 15:09:53 (jmiller)>

This is a piece of my pseudo-spectral shock capturing method, designed
to remove the Gibbs phenomenon from a set of modal data using the
Gegenbauer reconstruction method.
"""

# ======================================================================
# imports
# ======================================================================
import numpy as np
from orthopoly import PseudoSpectralStencil
from gegenbauer import GegenbauerReconstructor
# ======================================================================


def make_intervals(stencil,shock_positions):
    """
    Given a stencil (which contains x data) and the positions of discontinuities
    in the solution (assumed to be array-like), breaks up the domain into
    intervals over which the solution is assumed to be smooth and which
    are appropriate for a Gegenbauer reconstruction.
    """
    break_points = np.concatenate((np.array([stencil.xmin]),
                                   shock_positions,
                                   np.array([stencil.xmax])))
    intervals = np.empty((len(break_points)-1,2))
    for i in range(intervals.shape[0]):
        intervals[i,0] = break_points[i]
        intervals[i,1] = break_points[i+1]
    return intervals

def partiton_domain(intervals,x):
    """
    Partition x into pieces that live on intervals
    """
    

class GegenbauerReconstruction:
    """
    A convenience class that holds a Gegenbauer reconstruction. Can be
    called on an array (or on a float) and will evaluate the piecewise
    smooth reconstructed solution.
    """
    def __init__(self,
                 stencil,
                 reconstructor,
                 solution):
        """
        Initializes the Gegenbauer Reconstruction. Requires the stencil
        used to generate a solution, the solution itself, the
        Gegenbauer reconstructor object for this order, and the
        stencil used.

        These things are required for efficiency reasons only.
        """
        self.stencil = stencil
        self.xmin = self.stencil.xmin
        self.xmax = self.stencil.xmax
        self.reconstructor = reconstructor
        self.solution = solution
        self.scont = self.stencil.to_continuum(self.solution)
        self.shock_positions = self.stencil.get_shock_positions(self.solution)
        self.intervals = make_intervals(self.stencil,self.shock_positions)
        self.geg_solns \
            = np.array([self.reconstructor.reconstruct_solution(self.scont,
                                                                interval[0],
                                                                interval[1])\
                        for interval in self.intervals])
        self.x_partitioned = self.partition_domain()
        self.solution = self.evaluate_domain()

    def evaluate_point(self,x):
        """
        Given a point x, evaluates the reconstruction.
        """
        for i,interval in enumerate(self.intervals):
            if interval[0] <= x <= interval[1]:
                return self.geg_solns[i](np.array([x]))[0]
        raise ValueError("x not in any of the intervals.")

    def evaluate_interval(self,i):
        """
        Given an arbitrary interval i, evaluates the reconstruction on it.
        """
        return np.array([self.evaluate_point(x) for x in i])

    def partition_domain(self):
        """
        Partitions the domain of the input stencil and returns an array of many
        small intervals.
        """
        x = self.stencil.get_x()
        x_partitioned = np.array([filter(lambda xi: interval[0] <= xi < interval[1],x)\
                                  for interval in self.intervals])
        return x_partitioned

    def evaluate_domain(self):
        """
        Returns the reconstructed solution
        """
        yglist = [self.geg_solns[i](self.x_partitioned[i])\
                  for i in range(self.x_partitioned.shape[0])]\
                      +[self.geg_solns[-1](np.array([self.xmax]))]
#         yglist = [self.geg_solns[0](np.array([self.xmin]))]\
#                  +[self.geg_solns[i](self.x_partitioned[i])\
#                    for i in range(self.x_partitioned.shape[0])]\
#                        +[self.geg_solns[-1](np.array([self.xmax]))]
        out = np.concatenate(yglist)
        if out.shape != self.stencil.get_x().shape:
            print out.shape
            print self.stencil.get_x().shape
        return out

    def __call__(self,x=False):
        if not x:
            return self.evaluate_domain()
        else:
            return self.evaluate_point(x)


class TimeStepProjector:
    """
    A class that perfoms the Gegenbauer reconstruction at a
    time-step-by-time-step time scale.
    """
    def __init__(self,stencil,reconstructor):
        self.stencil = stencil
        self.reconstructor = reconstructor

    def __call__(self,ynew):
        reconstruction = GegenbauerReconstruction(self.stencil,
                                                  self.reconstructor,
                                                  ynew)
        return reconstruction.evaluate_domain()

    def on_arbitrary_interval(self,ynew,arbitrary_interval):
        reconstruction = GegenbauerReconstruction(self.stencil,
                                                  self.reconstructor,
                                                  ynew)
        return reconstruction.evaluate_interval(arbitrary_interval)

