#!/usr/bin/env python2

"""psuedospectral_shock.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-04-14 17:25:15 (jmiller)>

This is a little program to demonstrate shock capturing in
psuedospectral methods. Here I advect a square wave using the equation


(d/dt) u + (d/dx) u = 0

and find the discontinuity using nonlinearly enhanced spectral edge
detection and then use the Gegenbauer reprojection method to regain
spectral accuracy.

This file contains top-level functionality.
"""

# ======================================================================
# imports
# ======================================================================
import numpy as np
from orthopoly import PseudoSpectralStencil
from gegenbauer import GegenbauerReconstructor
from gibbs_remover import GegenbauerReconstruction,TimeStepProjector
import boundary
import evolution_systems as systems
import time_integration
import visualization
# ======================================================================

ORDER = 50
XMIN = -1
XMAX = 1
BOUNDARY_CONDITION = boundary.OneSidedPeriodicBoundaryCondition('left')
DIRICHLET_BOUNDARY = boundary.OneSidedDirichletBoundaryCondition(1)
#DIRICHLET_BOUNDARY = boundary.NoBoundaryCondition()
EVOLUTION_SYSTEM = systems.AdvectionSystem
T0 = 0
TMAX=2


def initialize_stencil_and_system(order=ORDER,
                                  xmin=XMIN,xmax=XMAX,
                                  boundary_condition=BOUNDARY_CONDITION,
                                  evolution_system = EVOLUTION_SYSTEM):
    stencil = PseudoSpectralStencil(order,xmin,xmax)
    evolution_system = EVOLUTION_SYSTEM(stencil,boundary_condition)
    return stencil,evolution_system

def advect_gaussian(order=ORDER,t0=T0,tmax=TMAX):
    stencil,evolution_system = initialize_stencil_and_system(order)
    initial_data_func = evolution_system.centered_gaussian_initial
    initial_data = systems.generate_initial_data(stencil.get_x(),
                                                 evolution_system,
                                                 initial_data_func)
    t_list,y_list = time_integration.integrate_system(t0,tmax,
                                                      initial_data,
                                                      stencil,
                                                      evolution_system,
                                                      BOUNDARY_CONDITION)
    return stencil,evolution_system,t_list,y_list

def advect_square_wave(order=ORDER,t0=T0,tmax=TMAX,post_processor=False,
                       output=False):
    stencil,evolution_system = initialize_stencil_and_system(order)
    initial_data_func = evolution_system.square_wave_initial
    initial_data = systems.generate_initial_data(stencil.get_x(),
                                                 evolution_system,
                                                 initial_data_func)
    if post_processor:
        reconstructor = GegenbauerReconstructor(int(stencil.order/2))
        projector = TimeStepProjector(stencil,reconstructor)
        p_processor = lambda t,x: np.array([projector(x[0])])
        t_list,y_list = time_integration.integrate_system(t0,tmax,
                                                          initial_data,
                                                          stencil,
                                                          evolution_system,
                                                          BOUNDARY_CONDITION,
                                                          p_processor,
                                                          output=output)
    else:
        t_list,y_list = time_integration.integrate_system(t0,tmax,
                                                          initial_data,
                                                          stencil,
                                                          evolution_system,
                                                          BOUNDARY_CONDITION,
                                                          output=output)
    return stencil,evolution_system,t_list,y_list

def advect_riemann(order=ORDER,t0=T0,tmax=TMAX,post_processor=False,
                       output=False):
    stencil,evolution_system \
        = initialize_stencil_and_system(order,
                                        boundary_condition=DIRICHLET_BOUNDARY)
    initial_data_func = evolution_system.riemann_problem_initial
    initial_data = systems.generate_initial_data(stencil.get_x(),
                                                 evolution_system,
                                                 initial_data_func)
    if post_processor:
        reconstructor = GegenbauerReconstructor(int(stencil.order/2))
        projector = TimeStepProjector(stencil,reconstructor)
        p_processor = lambda t,x: np.array([projector(x[0])])
        t_list,y_list = time_integration.integrate_system(t0,tmax,
                                                          initial_data,
                                                          stencil,
                                                          evolution_system,
                                                          DIRICHLET_BOUNDARY,
                                                          p_processor,
                                                          output=output)
    else:
        t_list,y_list = time_integration.integrate_system(t0,tmax,
                                                          initial_data,
                                                          stencil,
                                                          evolution_system,
                                                          DIRICHLET_BOUNDARY,
                                                          output=output)
    return stencil,evolution_system,t_list,y_list

def advect_sawtooth(order=ORDER,t0=T0,tmax=TMAX,post_processor=False,
                    output=False):
    stencil,evolution_system = initialize_stencil_and_system(order)
    initial_data_func = evolution_system.sawtooth_initial
    initial_data = systems.generate_initial_data(stencil.get_x(),
                                                 evolution_system,
                                                 initial_data_func)
    if post_processor:
        reconstructor = GegenbauerReconstructor(int(stencil.order/2))
        projector = TimeStepProjector(stencil,reconstructor)
        p_processor = lambda t,x: np.array([projector(x[0])])
        t_list,y_list = time_integration.integrate_system(t0,tmax,
                                                          initial_data,
                                                          stencil,
                                                          evolution_system,
                                                          BOUNDARY_CONDITION,
                                                          p_processor,
                                                          output=output)
    else:
        t_list,y_list = time_integration.integrate_system(t0,tmax,
                                                          initial_data,
                                                          stencil,
                                                          evolution_system,
                                                          BOUNDARY_CONDITION,
                                                          output=output)
    return stencil,evolution_system,t_list,y_list
