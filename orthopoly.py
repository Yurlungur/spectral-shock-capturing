#!/usr/bin/env python2

"""
orthopoly.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-04-20 04:07:25 (jmiller)>

A module for orthogonal polynomials for pseudospectral methods in Python
"""



# ======================================================================
# imports
# ======================================================================
import numpy as np
from numpy import polynomial
from numpy import linalg
from scipy import integrate
from scipy import optimize
from copy import copy
# ======================================================================



# ======================================================================
# Global constants
# ======================================================================
LOCAL_XMIN = -1 # Maximum and min values of reference cell
LOCAL_XMAX = 1
POLY = polynomial.chebyshev.Chebyshev  # A class for orthogonal Polynomials
WEIGHT_FUNC = polynomial.chebyshev.chebweight # Quadrature weight
#WEIGHT_FUNC = lambda x: 1.0/np.sqrt(1-x**2) # Quadrature weight
ORDER = 300
ALPHA = 6
INTEGRATOR=integrate.quad
ENHANCEMENT_P =5 
J_FACTOR=0.9
DEFAULT_RESOLUTION=100 # default resolution for plots
EDGE_CUTOFF = 4 # Number of significant digits for edge detection
EDGE_EPSILON=0.1
# ======================================================================



# ======================================================================
# Utilities
# ======================================================================
def get_norm2_difference(foo,bar,xmin,xmax):
    """
    Returns sqrt(integral((foo-bar)**2)) on the interval [xmin,xmax]
    """
    out = INTEGRATOR(lambda x: (foo(x)-bar(x))**2,xmin,xmax)[0]
    out /= float(xmax-xmin)
    out = np.sqrt(out)
    return out
# ======================================================================



# ======================================================================
# Nodal and Modal Details
# ======================================================================
def continuous_inner_product(foo,bar):
    """"
    Takes the continuous inner product in the POLY norm between
    the functions foo and bar.
    """
    return INTEGRATOR(lambda x: foo(x)*bar(x)*WEIGHT_FUNC(x),
                      LOCAL_XMIN,LOCAL_XMAX,
                      epsrel=1E-2/ORDER)[0]

def get_quadrature_points(order):
    """
    Returns the quadrature points for Gauss-Lobatto quadrature
    as a function of the order of the polynomial we want to
    represent.
    See: https://en.wikipedia.org/wiki/Gaussian_quadrature
    """
    return np.sort(np.concatenate((np.array([-1,1]),
                                   POLY.basis(order).deriv().roots())))

def get_vandermonde_matrices(order,nodes=False):
    """
    Returns the Vandermonde fast-Fourier transform matrices s2c and c2s,
    which convert spectral coefficients to configuration space coefficients
    and vice-versa respectively. Requires the order of the element/method
    as input.
    """
    if np.any(nodes == False):
        nodes = get_quadrature_points(order)
    s2c = np.zeros((order+1,order+1),dtype=float)
    for i in range(order+1):
        for j in range(order+1):
            s2c[i,j] = POLY.basis(j)(nodes[i])
    c2s = linalg.inv(s2c)
    return s2c,c2s

# cute but doesn't work. Not sure why.
# def get_weights(order,c2s = False, nodes = False):
#     """
#     Calculates the integration weights for the orthogonal function
#     inner product. Needs the order of the method and the Vandermonde
#     matrix that maps the colocation representation to a spectral
#     representation.
#     """
#     if np.any(c2s == False):
#         s2c,c2s = get_vandermonde_matrices(order,nodes)
#     integrals = np.empty((order+1),dtype=float)
#     for i in range(order+1):
#         integrals[i] = continuous_inner_product(lambda x: x,POLY.basis(i))
#     weights = np.dot(c2s.transpose(),integrals)
#     return weights

def get_weights(order,c2s = False, nodes=False):
    """
    Returns the integration weights for Gauss-Lobatto quadrature
    as a function of the order of the polynomial we want to
    represent.
    See: https://en.wikipedia.org/wiki/Gaussian_quadrature
    See: arXive:gr-qc/0609020v1
    """
    if POLY == polynomial.chebyshev.Chebyshev:
        weights = np.empty((order+1))
        weights[1:-1] = np.pi/order
        weights[0] = np.pi/(2*order)
        weights[-1] = weights[0]
        return weights
    elif POLY == polynomial.legendre.Legendre:
        if np.all(nodes == False):
            nodes=get_quadrature_points(order)
        interior_weights = 2/((order+1)*order*POLY.basis(order)(nodes[1:-1])**2)
        boundary_weights = np.array([1-0.5*np.sum(interior_weights)])
        weights = np.concatenate((boundary_weights,
                                  interior_weights,
                                  boundary_weights))
        return weights
    else:
        raise ValueError("Not a known polynomial type.")
        return False
    
def get_modal_differentiation_matrix(order):
    """
    Returns the differentiation matrix for the first derivative in the
    modal basis.
    """
    out = np.zeros((order+1,order+1))
    for i in range(order+1):
        out[:i,i] = POLY.basis(i).deriv().coef
    return out

def get_nodal_differentiation_matrix(order,
                                     s2c=False,c2s=False,
                                     Dmodal=False):
    """
    Returns the differentiation matrix for the first derivative
    in the nodal basis

    It goes without saying that this differentiation matrix is for the
    reference cell.
    """
    if np.any(Dmodal == False):
        Dmodal = get_modal_differentiation_matrix(order)
    if np.any(s2c == False) or np.any(c2s == False):
        s2c,c2s = get_vandermonde_matrices(order)
    return np.dot(s2c,np.dot(Dmodal,c2s))
# ======================================================================



# ======================================================================
# Operators outside reference cell
# ======================================================================
def get_colocation_points(order,xmin=LOCAL_XMIN,xmax=LOCAL_XMAX,
                          quad_points=False):
    """
    Generates order+1 colocation points on the domain [xmin,xmax]
    """
    if np.any(quad_points == False):
        quad_points = get_quadrature_points(order)
    scale_factor = (xmax-float(xmin))/(LOCAL_XMAX-float(LOCAL_XMIN))
    shift_factor = xmin-float(LOCAL_XMIN)
    return scale_factor*(shift_factor + quad_points)

def get_global_differentiation_matrix(order,
                                      xmin=LOCAL_XMIN,
                                      xmax=LOCAL_XMAX,
                                      s2c=False,
                                      c2s=False,
                                      Dmodal=False):
    """
    Returns the differentiation matrix in the nodal basis
    for the global coordinates (outside the reference cell)

    Takes the Jacobian into effect.
    """
    scale_factor = (xmax-float(xmin))/(LOCAL_XMAX-float(LOCAL_XMIN))
    LD = get_nodal_differentiation_matrix(order,s2c,c2s,Dmodal)
    PD = LD/scale_factor
    return PD
# ======================================================================



# ======================================================================
# Reconstruct Global Solution
# ======================================================================
def get_continuous_object(grid_func,
                          xmin=LOCAL_XMIN,xmax=LOCAL_XMAX,
                          c2s=False):
    """
    Maps the grid function grid_func, which is any field defined
    on the colocation points to a continuous function that can
    be evaluated.

    Parameters
    ----------
    xmin -- the minimum value of the domain
    xmax -- the maximum value of the domain
    c2s  -- The Vandermonde matrix that maps the colocation representation
            to the spectral representation

    Returns
    -------
    An numpy polynomial object which can be called to be evaluated
    """
    order = len(grid_func)-1
    if np.any(c2s == False):
        s2c,c2s = get_vandermonde_matrices(order)
    spec_func = np.dot(c2s,grid_func)
    my_interp = POLY(spec_func,domain=[xmin,xmax])
    return my_interp
# ======================================================================



# ======================================================================
# Concentration Factors and Spectral Edge Detection
# ======================================================================
"""
Given a spectral represenatation of a function
f(x) = sum_{n=0}^N f_hat_n P_n(x)

define the jump function
[f](x) = f(x^+) - f(x^-),
which at each point x, converges to the difference between the left- and
right-hand limits of f.

Then the following result holds:
| (pi*sqrt(1-x^2)/N) sum_{n=1}^N mu(k/N) f_hat_n P_n'(x) - [f](x) | <= C log(N)/N

where mu(xi) is a so-called admissable concentration factor, which must be odd and
normalized to 1 on the interval [0,1], C is a constant, and P_n' is the derivative
of the nth basis function.

See:
    Gelb and Tadmoor 2001:
       Detection of Edges in Spectral Data II . Nonlinear Enhancement
"""
def concentration_exponential(alpha,xi):
    """
    Given the constant alpha and the variable xi in the interval [0,1],
    returns the (un-normalized) exponential concentration factor. See
    Gelb and Tadmoor 2001:
       Detection of Edges in Spectral Data II . Nonlinear Enhancement

    Note the concentration factor must be explicitly set to return 0
    for xi == 0 or xi == 1 because it is discontinuous at these points but
    we are interested in the limits interior to the interval [0,1].
    """
    if xi == 0 or xi == 1:
        return 0
    else:
        return np.exp(1.0/(alpha*xi*(xi-1)))
            
def concentration_normalization(alpha=ALPHA):
    """
    The normalization factor for the exponential concentration factor given above.
    """
    return integrate.quad(lambda eta: concentration_exponential(alpha,eta),0,1)[0]

def get_concentration_factors(order,alpha=ALPHA):
    """
    Given an order of the method, returns a diagonal matrix containing
    concentration factors to be used on the concentration kernel or to
    be used in the concentration operation.
    """
    cnorm = concentration_normalization(alpha)
    factors = np.empty((order+1))
    dx = (2*np.pi)/float(2*order+1)
    for i in range(order+1):
        factors[i] = concentration_exponential(alpha,float(i)/order)
        # nodal conversion factor
        #if i != 0:
        #    factors[i] *= (np.sin(0.5*i*dx)/(0.5*i*dx))
    factors *= (1.0/cnorm)
    return np.diag(factors)

def concentration_kernel_prefactor(order,nodes=False):
    """
    The term in front of the filtered derivative of the solution.
    Requires the order.
    """
    if np.any(nodes == False):
        nodes = get_quadrature_points(order)
    #return np.diag(np.ones_like(nodes))
    return np.diag((np.pi*np.sqrt(1+EDGE_EPSILON-nodes**2))/order)

def get_edge_detection_operator(order,nodes=False,
                                s2c=False,c2s=False,
                                Dmodal=False,
                                concentration_factors=False,
                                ck_prefactor=False,
                                alpha=ALPHA):
    """
    Returns an operator that, when applied to the nodal representation
    of the function, concentrates it to find the edge in the nodal
    space. The concentrated solution still needs nonlinear enhancement
    """
    if np.any(nodes == False):
        nodes = get_quadrature_points(order)
    if np.any(s2c==False) or np.any(c2s == False):
        s2c,c2s = get_vandermonde_matrices(order,nodes)
    if np.any(Dmodal==False):
        Dmodal = get_modal_differentiation_matrix(order)
    if np.any(ck_prefactor==False):
        ck_prefactor = concentration_kernel_prefactor(order,nodes)
    if np.any(concentration_factors==False):
        concentration_factors = get_concentration_factors(order,alpha)
    op = np.dot(ck_prefactor,
                np.dot(s2c,
                       np.dot(Dmodal,
                              np.dot(concentration_factors,
                                     c2s))))
    return op

def enhancement_epsilon(n):
    """
    Needed for nonlinearly enhance edge below
    """
    return (1.0/n)

def get_enhancement_j(n):
    """
    Returns the power of J to use
    """
    if n == 1:
        out= n
    elif n == 2:
        out= n*(n-1)
    else:
        out= n*(n-2)
    out = n**2
    return J_FACTOR*out

def nonlinearly_enhance_edge(solution,edges):
    """
    Given a nodal representation of the solution and the edges,
    returns a nonlinearly enhanced modal edge detector.
    """
    amplitude = 1#np.max(solution) - np.min(solution)
    nmax=len(edges)
    epsilon = enhancement_epsilon(nmax)
    j = amplitude*get_enhancement_j(nmax)
    p = ENHANCEMENT_P
    amplified_edges = (epsilon**(-p/2.0))*np.absolute(edges)**p
    new_edges = copy(edges)
    new_edges[amplified_edges <= j] = 0
    return new_edges

def get_nonlinearly_enhanced_edge_func(solution,edge_func):
    """
    Given the continuum edge detector calulated by getting a continuum
    object from the result of an edge-detection operation, nonlinearly
    enhance it.

    Requires the solution, which should be in the array-like nodal
    representation, and the edge detector function.
    """
    amplitude = np.max(solution) - np.min(solution)
    nmax=len(solution)
    epsilon = enhancement_epsilon(nmax)
    j = amplitude*get_enhancement_j(nmax)
    p = ENHANCEMENT_P
    def enhanced_edge_func(x):
        if (epsilon**(-p/2.0))*np.absolute(edge_func(x))**p > j:
            return edge_func(x)
        else:
            return 0
    return enhanced_edge_func
# ======================================================================


# ======================================================================
# A convenience class that generates everything and can be called
# ======================================================================
class PseudoSpectralStencil:
    """
    A convenience class. Given an order, and a domain [xmin,xmax]
    defines internally all structures and methods the user needs.
    """
    def __init__(self,order,xmin,xmax,alpha=ALPHA):
        """
        Constructor. Needs the order of the method and the domain
        [xmin,xmax].
        """
        self.order = order
        self.xmin = xmin
        self.xmax = xmax
        self.quads = get_quadrature_points(self.order)
        self.weights = get_weights(self.order,self.quads)
        self.s2c,self.c2s = get_vandermonde_matrices(self.order,self.quads)
        self.Dmodal = get_modal_differentiation_matrix(self.order)
        self.Dnodal = get_nodal_differentiation_matrix(self.order,
                                                       self.s2c,self.c2s,
                                                       self.Dmodal)
        self.colocation_points = get_colocation_points(self.order,
                                                       self.xmin,self.xmax,
                                                       self.quads)
        self.PD = get_global_differentiation_matrix(self.order,
                                                    self.xmin,self.xmax,
                                                    self.s2c,self.c2s,
                                                    self.Dmodal)
        self.conc_factors = get_concentration_factors(self.order,
                                                      alpha)
        self.ck_prefactor = concentration_kernel_prefactor(self.order,
                                                           self.quads)
        self.edge_op = get_edge_detection_operator(self.order,
                                                   self.quads,
                                                   self.s2c,self.c2s,
                                                   self.Dmodal,
                                                   self.conc_factors,
                                                   self.ck_prefactor,
                                                   alpha)

    def get_x(self):
        """
        Returns the colocation points
        """
        return self.colocation_points


    def differentiate(self,grid_func,order=1):
        """
        Given a grid function defined on the colocation points,
        returns its derivative of the appropriate order
        """
        assert type(order) == int
        assert order >= 0
        if order == 0:
            return grid_func
        else:
            return self.differentiate(np.dot(self.PD,grid_func),order-1)

    def to_continuum(self,grid_func):
        """
        Given a grid function defined on the colocation points, returns a
        numpy polynomial object that can be evaluated.
        """
        return get_continuous_object(grid_func,self.xmin,self.xmax,self.c2s)

    def make_plottable(self,grid_func,resolution=DEFAULT_RESOLUTION):
        """
        Given a grid function, returns evenly spaced x and y data 
        """
        return self.to_continuum(grid_func).linspace(resolution)

    def find_edge(self,grid_func,enhancement=True):
        """
        Given a grid function defined on the colocation points, returns
        a new grid function with the (nonlinearly enhanced) edge.
        """
        edge_function = np.dot(self.edge_op,grid_func)
        if enhancement:
            edge_function = nonlinearly_enhance_edge(grid_func,edge_function)
        return edge_function

    def get_edge_detector(self,grid_func,enhancement=True):
        """
        Given a grid function, returns an edge detector function which
        converges to the difference between the left- and right-hand limits
        of the the grid function at each point (in the continuum).

        If enhancement == True, will nonlinearly enhance.
        """
        if not enhancement:
            return self.to_continuum(self.find_edge(grid_func,enhancement))

    def get_shock_positions(self,grid_func,enhancement=True,
                            debugging=False,
                            edge_cutoff = EDGE_CUTOFF):
        """
        Given a grid function (with assumed discontinuities), calculates
        the positions of those shocks using nonlinearly enhanced
        spectral edge detection (to get the concentration function)
        and the simplex algorithm ot find the shocks.
        """
        edge = self.find_edge(grid_func,enhancement)
        nonzero_xs = self.get_x()[edge != 0]
        if debugging:
            print nonzero_xs
        if len(nonzero_xs) == 0: # i.e., if there are no edges
            return np.array([])
            
        edge_func = self.get_edge_detector(grid_func,False)
        if enhancement:
            edge_func = get_nonlinearly_enhanced_edge_func(grid_func,
                                                           edge_func)
        f = lambda x: -np.abs(edge_func(x))
        possible_edges = np.array([optimize.fmin(f,x,
                                                 xtol=1.0/(10**(edge_cutoff)),
                                                 disp=False)\
                                   for x in nonzero_xs])
        possible_edges = np.array(sorted(list(set(np.around(possible_edges.flatten(),
                                                            edge_cutoff-1)))))
        return possible_edges

    def __call__(self,grid_func):
        "Convenience call to first-order differentiation."
        return self.differentiate(grid_func)
# ======================================================================


