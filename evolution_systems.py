#!/usr/bin/env python2

"""evolution_systems.py
Author: Jonah Miller (jonah.maxwell.miller@gmail.com)
Time-stamp: <2015-04-19 22:41:03 (jmiller)>

This is part of the pseudospectral_shock library. It
implements evolution equations/right-hand-sides for systems.
"""

# ======================================================================
# imports
# ======================================================================
import numpy as np
from orthopoly import PseudoSpectralStencil
import boundary
import abc
# ======================================================================


# ======================================================================
# Global constants
# ======================================================================
ADVECTION_SPEED = 1
AMPLITUDE = 1
GAUSS_WIDTH = 0.1
SQUARE_WIDTH = 0.4
# ======================================================================

def signed_mod(x,y):
    return np.sign(x)*np.mod(np.abs(x),y)

class EvolutionSystem:
    """
    An abstract base class for an evolution system.

    An evolution system must provide the following:
    -- The definition of a state vector. This should
       be a numpy array of strings with the names of each of the
       variables in the state vector.
    -- a right-hand-side rhs that takes in a state-vector
       and returns a right-hand-side.
    -- A default analytic function for the initial data.
       default_inital. Should be a function of x and return
       a 1d array where the index gives
       each of the variables in the state-vector.
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def state_vector(self):
        """
        Returns an array with the names of all variables in
        the state vector.
        """

    @abc.abstractmethod
    def rhs(self,t,state_vector):
        """
        Returns the rhs for a given state vector at time t.
        """

    @abc.abstractmethod
    def default_initial(self,x):
        """
        Given a position x, returns a 1D array
        with the state vector at time 0.
        """

    @abc.abstractmethod
    def num_variables(self):
        """
        Returns an integer giving the number of evolution
        variables
        """

    def __call__(self,state_vector):
        "Convenience access to rhs"
        return self.rhs(state_vector)

class AdvectionSystem(EvolutionSystem):
    """
    An evolution system for advection. Obeys the equation
    (d/dt) u + c (d/dx) u = 0

    In this case, the state vector is only one variable long.
    """
    def __init__(self,stencil,boundary_condition,
                 c=ADVECTION_SPEED):
        """
        Initiates an advection system with advection
        speed c.

        stencil is the pseudospectral stencil to use.
        """
        self.c = c
        self.stencil = stencil
        self.n_vars = 1
        self.boundary_condition = boundary_condition

    def num_variables(self):
        """
        Returns an integer giving the number of evolution
        variables
        """
        return self.n_vars

    def rhs(self,t,state_vector):
        "The right-hand side for a given state vector."
        u = state_vector[0]
        du = self.stencil.differentiate(u)
        du = self.boundary_condition.enforce(du)
        return -self.c*np.array([du])

    def state_vector(self):
        """
        The names of the variables in the advection system.
        """
        return np.array(['u'])

    def centered_gaussian(self,x,t):
        """
        centered gaussian analytic solution
        """
        xmin = self.stencil.xmin
        xmax = self.stencil.xmax
        x0 = 0.5*(xmin+xmax)
        a = AMPLITUDE
        sigma = GAUSS_WIDTH
        xshifted = np.mod(x-xmin-t,xmax-xmin)
        u = a*np.exp(-(xshifted+xmin-x0)**2/(2*sigma**2))
        return np.array([u])

    def centered_gaussian_initial(self,x):
        """
        centered gaussian initial data
        """
        return self.centered_gaussian(x,0)

    def square_wave(self,x,t):
        """
        Square wave analytic solution
        """
        xmin = self.stencil.xmin
        xmax = self.stencil.xmax
        x0 = 0.5*(xmin+xmax) #- 0.25*(xmax-xmin)
        xmod = np.mod(x-xmin-t,xmax-xmin) + xmin
        if x0-0.5*SQUARE_WIDTH <= xmod <= x0+0.5*SQUARE_WIDTH:
            u = AMPLITUDE
        else:
            u = 0
        return np.array([u])

    def square_wave_initial(self,x):
        """
        Square wave initial data
        """
        return self.square_wave(x,0)

    def riemann_problem(self,x,t):
        """
        Analtyic solution for a single advecting shock
        """
        xmin = self.stencil.xmin
        xmax = self.stencil.xmax
        x0 = xmin + 0.1*(xmax - xmin)
        xshifted = x-xmin-t
        u = AMPLITUDE if xshifted + xmin <= x0 else 0
        return np.array([u])

    def riemann_problem_initial(self,x):
        """
        Initial data for a single advecting shock
        """
        return self.riemann_problem(x,0)

    def sawtooth_initial(self,x):
        """
        Initial data for a sawtooth wave
        """
        xmin = self.stencil.xmin
        xmax = self.stencil.xmax
        x0 = 0.5*(xmin+xmax)
        if x < x0:
            u = -(x-xmin)
        else:
            u = -(x-xmax)
        return np.array([u])

    def default_initial(self,x):
        """
        Default initial data. For now uses centered_gaussian
        """
        return self.centered_gaussian_initial(x)


def generate_initial_data(x,evolution_system,initial_data_func):
    """
    Given a function that generates initial data, as described above,
    generates a numpy array containing the initial data evaluated at
    x.

    Returns an array-like object where the rows correspond to x and the colums
    correspond to the elements of the state vector
    """
    out = np.empty((evolution_system.num_variables(),len(x)))
    for i in range(len(x)):
        out[...,i] = initial_data_func(x[i])
    return out

    
def get_analytic_solution(x,evolution_system,analytic_solution_func,t):
    """
    Given a function returns the analytic solution of the evolution system
    given by analytic_solution_func, which must be compatible with the evolution
    system.

    The analytic_solution_func is assumed to be a function of x and t in that order.
    """
    return generate_initial_data(x,evolution_system,lambda x: alaytic_solution_func(x,t))
    
